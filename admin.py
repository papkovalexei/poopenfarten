from fastapi import FastAPI
from mongoengine import connect
from pydantic import BaseModel

from repository import models


connect("poopenfarten", host="192.168.1.68")


class ItemCost(BaseModel):
    Lvl: int
    Cost: int
    Profit: int


app = FastAPI()


@app.get("/list_costs")
def read_costs() -> list[ItemCost]:
    result = []
    for cost in models.ToiletCost.objects:
        result.append(ItemCost(
            Lvl=cost.lvlNumber,
            Cost=cost.lvlCost,
            Profit=cost.profit
        ))
    return result


@app.post("/add_costs")
def add_costs(item: ItemCost):
    return models.ToiletCost.objects(lvlNumber=item.Lvl).update_one(upsert=True, set__lvlCost=item.Cost, set__profit=item.Profit)
