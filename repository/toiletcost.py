from repository import models


class Cost:
    Lvl: int
    Cost: int

    def __init__(self, lvl, cost):
        self.Lvl = lvl
        self.Cost = cost


def get_costs() -> list[Cost]:
    result = []
    for cost in models.ToiletCost.objects:
        result.append(Cost(
            lvl=cost.lvlNumber,
            cost=cost.lvlCost
        ))
    return result
