from mongoengine import Document, IntField, ReferenceField


class ToiletCost(Document):
    lvlNumber = IntField(requiered=True, unique=True)
    lvlCost = IntField(requiered=True)
    profit = IntField(requiered=True)


class User(Document):
    userID = IntField(requiered=True)
    chatID = IntField(requiered=True)
    money = IntField(default=0)
    toilet = ReferenceField(ToiletCost, requiered=True)

    meta = {'indexes': ['userID', 'chatID']}


