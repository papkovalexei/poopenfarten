import config

from aiogram import executor
import handlers

if __name__ == '__main__':
    config.dp.register_message_handler(handlers.upgrade_handler,
                                       commands=['upgrade'])
    config.dp.register_message_handler(handlers.shit_handler,
                                       regexp='^.*срать.*$')
    config.dp.register_message_handler(handlers.list_cost,
                                       commands=['cost'])
    config.dp.register_message_handler(handlers.info_handler,
                                       commands=['info'])
    executor.start_polling(config.dp)
