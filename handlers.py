import config

from aiogram import types
from repository.toiletcost import get_costs
from repository.user import find_user, shit, upgrade


async def list_cost(message: types.Message):
    costs = get_costs()
    result_msg = ""

    for cost in costs:
        result_msg += "Уровень толчка🚽: " + str(cost.Lvl) + " - " + str(cost.Cost) + "💰\n"

    await message.reply(result_msg)


async def shit_handler(message: types.Message):
    result = shit(message.from_user.id, message.chat.id)

    await message.reply(f"{message.from_user.full_name} насрал {result.New} говна!💩 у тебя {result.Current} говна")


async def upgrade_handler(message: types.Message):
    result = upgrade(message.from_user.id, message.chat.id)

    if result == -1:
        await message.reply("Твой туалет слишком крут")
        return
    elif result == -2:
        await message.reply("ты бомж тупой")
        return
    await message.reply("Твой туалет стал еще круче")


async def info_handler(message: types.Message):
    user = find_user(message.from_user.id, message.chat.id)
    await message.reply(f"Прокачка туалета: {user.toilet.lvlNumber}, денег: {user.money}")
